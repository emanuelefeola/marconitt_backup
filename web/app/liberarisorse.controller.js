app
    .controller('LiberaRisorseCtrl', function($scope, $timeout, $http, $mdToast, $rootScope, CONFIG, $mdDialog) {

        $scope.classes; // list of classes
        $scope.event = {};
        $scope.event.currentDate;
        $scope.event.description; // description of the event
        $scope.event.dayStart = new Date(); // day of the event
        $scope.event.dayEnd = new Date(); // day of the event
        $scope.event.class; // selected class of the event
        $scope.event.ore; // selected hours of the event
        $scope.isLoading = true; // used for loading circle


        /**
         * Initialization method
         */
        var init = function() {
            $http.get('http://' + CONFIG.HOST + ':' + CONFIG.PORT + '/default')
                .success(function(response) {

                    $scope.classes = response.classes;
                    $timeout(function() {
                        $scope.isLoading = false;
                    }, $rootScope.loadingTime);

                }).error(function() {
                    $mdToast.show($mdToast.simple().textContent("Errore di rete!"));
                });
        };

        $scope.manageInsert2 = function () {
            console.log($scope.event.dayStart);
            console.log($scope.event.dayEnd);
            
            $year = $scope.event.dayStart.getFullYear();
            $month = $scope.event.dayStart.getMonth();
            $day = $scope.event.dayStart.getDate();
            console.log($day);
            //console.log(new Date($year, $month, ++$day));
        };
        
        $scope.manageInsert = function () {
            $scope.event.currentDate = $scope.event.dayStart;
            $scope.year = $scope.event.dayStart.getFullYear();
            $scope.month = $scope.event.dayStart.getMonth();
            $scope.day = $scope.event.dayStart.getDate();
            console.log('0');
            if($scope.event.currentDate.valueOf() == $scope.event.dayEnd.valueOf()){
                $scope.insert();
            } else{
                while($scope.event.currentDate.valueOf() <= $scope.event.dayEnd.valueOf()){
                    console.log('1');
                    //console.log($scope.event.currentDate);
                    $scope.insert();
                    console.log($scope.day);
                    $scope.day++;
                    $scope.event.currentDate = new Date($scope.year, $scope.month, $scope.day);
                }
            }
        };

        /**
         * sends event information to server
         */
        $scope.insert = function () {
            console.log('2');
            
            //control if selected date is not holiday
            for (var i=0; i<date.length; i++)
            // for data in date:
                
            var data = new Date(date[i]);
            var giorno = data.getDate();
            var mese = data.getMonth() + 1;
            var anno = data.getFullYear();
            var n_date = anno + "-" + mese + "-" + giorno;

            $http.get('http://' + CONFIG.HOST + ':' + CONFIG.PORT + '/isholiday', {
                cache: false,
                params: {
                    day: n_date
                }
            }).success(function(response) {

                if (response) {
                    //date is holiday
                    $mdToast.show($mdToast.simple().textContent("Non si può liberare in un giorno di vacanza"));
                } else {
                    //date is not holiday
                    var confirm = $mdDialog.confirm()
                    .textContent('La risorsa verrà liberata per sempre. Continuare?')
                    .ok('CONFERMA')
                    .cancel('ANNULLA');

                    $mdDialog.show(confirm).then(function() {
                        console.log('££££££££££££££££££££££££££££££££');
                        console.log($scope.day);
                        var desc = $scope.event.description;
                        var day = date[i].getFullYear() + "-" + (date[i].getMonth()+1) + "-" + date[i].getDate();
                        // var day = $scope.event.currentDate.getFullYear() + "-" + ($scope.event.currentDate.getMonth()+1) + "-" + $scope.event.currentDate.getDate();
                        //var sClass = $scope.event.class;
                        //var sOre = $scope.event.ore;
                        var data = "descrizione="+desc+"&day="+day+"&classe="+$scope.event.class+"&ore="+$scope.event.ore+"&token="+sessionStorage.token;

                        var req = {
                            method: 'POST',
                            url: 'http://' + CONFIG.HOST + ':' + CONFIG.PORT + '/api/liberaRisorse',
                            data: data,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        };

                        $http(req)
                            .then(
                                function(data) {
                                    if (data.data) {
                                        $mdToast.show($mdToast.simple().textContent("Risorsa liberata con successo"));
                                        //$scope.event = {};
                                        //$date = $scope.event.currentDate
                                        //$scope.event.currentDate = new Date($data.getFullYear, $data.getMonth, ++$data.getDate);
                                    } else
                                        $mdToast.show($mdToast.simple().textContent("Errore durante la liberazione"));
                                },
                                function(err) {
                                    $mdToast.show($mdToast.simple().textContent("Errore di rete!"));
                                }
                            );
                    });
                }
            }).error(function() {
                $mdToast.show($mdToast.simple().textContent("Errore di rete!"));
            });
            //fine for
        };


        /**
         * says if a day is sunday or not
         * @param date
         * @returns {boolean}
         */
        $scope.noSunday = function(date) {
            var day = date.getDay();
            return day !== 0;
        };


        // on start
        init();
    });